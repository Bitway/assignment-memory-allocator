
CC = gcc
BUILDDIR = build
OBJDIR = obj
SRCDIR = src



INCLUDEDIR =
CFLAGS = -c -std=c99 -I$(INCLUDEDIR) -ggdb -Wextra
LDFLAGS	=
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(patsubst ${SRCDIR}%.c,${OBJDIR}%.o,$(SOURCES))
TARGET = memory-allocator

all: $(BUILDDIR) $(OBJDIR) $(BUILDDIR)/$(TARGET) $(OBJECTS)

$(BUILDDIR)/$(TARGET): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

$(OBJECTS): $(OBJDIR)/%.o:$(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(BUILDDIR):
	mkdir -p $@
$(OBJDIR):
	mkdir -p $@

clean:
	rm -rf $(BUILDDIR) $(OBJDIR)
.PHONY: clean
