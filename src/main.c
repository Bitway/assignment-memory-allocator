#include "mem.h"
#include "util.h"
#include "mem_internals.h"
#include <stdio.h>
#include <stdlib.h>

#define HEAP_SIZE 8192
void *heap = NULL;


int main() {

    heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    printf("TEST1: _malloc function\n");
    const size_t query = 64;
    void *memory = _malloc(query);
    if (memory != NULL) {
        printf("Success memory allocation on %p address \n", memory);
        printf("Try to free function:\n");
        debug_heap(stdout, heap);
        debug_struct_info(stdout, memory);
        _free(memory);
        debug_heap(stdout, heap);
    } else {
        printf("???\n");
    }

    printf("--------\n\n");

    printf("TEST2:\n");
    size_t query1 = 128;
    size_t query2 = 256;
    size_t query3 = 512;
    void *memory1 = _malloc(query1);
    void *memory2 = _malloc(query2);
    void *memory3 = _malloc(query3);
    debug_heap(stdout, heap);
    _free(memory2);
    debug_heap(stdout, heap);

    printf("TEST3:\n");
    memory2 = _malloc(query2);
    _free(memory3);
    _free(memory1);
    debug_heap(stdout, heap);
    _free(memory2);
    debug_heap(stdout, heap);

    printf("TEST4:\n");
    query1 = 4096;
    query2 = query1 * 2;
    memory1 = _malloc(query1);
    memory2 = _malloc(query2);
    debug_heap(stdout, heap);
    _free(memory1);
    _free(memory2);
    debug_heap(stdout, heap);
    return 0;
}
